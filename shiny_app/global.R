# global.R

# This file will run once upon start of the app so that it can ask the user to input a file, and then run that bash script.
# At least, I think that is how I should approach this at first. Actually, there is probably a better way.

## ------------------------------------------------------- ##

# # install packages if not installed
# packages <- c("ggmap", "shiny", "shinyjs", "mapproj", "scales")
# 
# # selection.vector vector indicating which packages are installed
# selection.vector <- packages %in% rownames(installed.packages())
# if (all(selection.vector)) {
#   # if they're all installed, load them
#   invisible(lapply(packages, require, character.only=TRUE))
# } else {
#   # install the missing packages
#   invisible(lapply(packages[selection.vector], install.packages))
#   # now load the newly installed packages
#   invisible(require(packages))
# }

#----------------------------------------------------------##

library(ggmap)
library(mapproj)

cleanUp <- function(bigDF) {
  # This function takes the main data frame, and cleans
  # it up and adds the necssary columns for mapping.
  
  # I was going to sit down and optimize this code, but I just want to finish this app and turn it in.
  # I'll optimize it later
  
  # Next few lines split the location column with coord pairs into two columns, lat & lon
  loc = as.data.frame(bigDF$Location)
  colnames(loc) <- "latlon"
  latlon <- data.frame(do.call('rbind', strsplit(as.character(loc$latlon), ' ', fixed = TRUE) ), stringsAsFactors = FALSE)
  colnames(latlon) <- c('lat', 'lon')
  bigDF <- cbind(bigDF, latlon)
  bigDF$Location <- NULL
  
  rm(loc, latlon)
  
  # Convert lat and lon to numerics
  bigDF$lat <- as.numeric(bigDF$lat)
  bigDF$lon <- as.numeric(bigDF$lon)
  
  # parse datetime column as posix times
  mytime <- as.vector(bigDF$Time)
  datetime <- as.POSIXct(strptime(mytime, "%Y-%m-%d %H:%M:%S"))
  bigDF <- cbind(bigDF, datetime)
  bigDF$Time <- NULL
  
  rm(datetime, mytime)
  
  # Give each day a unique id
  tout <- as.matrix(lapply(bigDF$datetime, strftime, "%Y-%m-%d"))
  bigDF$uniqueDay <- as.character(tout)
  rm(tout)
  
  # remake uniqueDay column to just integers
  # get unique days
  ud <- unique(bigDF$uniqueDay)
  udf <- as.data.frame(ud, stringsAsFactors = FALSE) # missing parameter: col.names = c('uniqueDay') see below comment
  colnames(udf) <- c("uniqueDay") # why doesn't the parameter in the above work?
  udf$day_index <- seq.int(length.out = length(x = ud))
  bigDF <- merge(x = bigDF, udf, by = "uniqueDay")
  rm(ud, udf)
  
  # project the coordinates
  # assuming that the coordinates will come from the united states --> use Albers
  
  locProj <- mapproject(bigDF$lat, bigDF$lon, "albers", par = c(37, 37.5))
  bigDF$latproj <- locProj$x
  bigDF$lonproj <- locProj$y
  
  rm(locProj)
  
  # Finished processing, return the processed data frame
  
  return(bigDF)
}

# Subset the map based on location

# Subset the map based on time

dateSubset <- function(pdf, start, end) {
  # Takes a processed df (pdf)
  # then subsets based on the date picker ranges
  
  #start <- as.character(start)
  #end <- as.character(end)
  
  out <- subset(pdf, datetime > start & datetime < end)
  return(out)
}

# test function to get bbox
tbbox <- function(thedata) {
  bbox <- as.numeric(c(min(thedata$lonproj), min(thedata$latproj), max(thedata$lonproj), max(thedata$latproj)))
  return(bbox)
}

# Do the mapping!

detailMap <- function(thedata, the_color) {
  bbox <- as.numeric(c(min(thedata$lonproj), min(thedata$latproj), max(thedata$lonproj), max(thedata$latproj)))
  paste(bbox)
  # bbox <- as.numeric(c(-88.09026,  25.85763, -80.22366,  42.41029))
  basemap <- get_map(location=bbox, source='google', maptype="terrain", color="bw")
  ggmap(basemap) + geom_path(aes(x=lon, y=lat, group=day_index), size=0.9, color=the_color, alpha=0.9, data=thedata) # color can be anything
}