# test_stock.R

library(shiny)
library(quantmod)
source("stockVis/helpers.R")

# server.R

server <- shinyServer(function(input, output) {
  
  dataInput <- reactive({ # is data input a function now? - The reactive expression will only return the saved result if it knows that the result is up-to-date.
    getSymbols(Symbols = input$symb, # the reactive function will check, symbol, src, from, to, and auto.assign and figure out that everything has remained the same
               src = "yahoo",
               from = input$dates[1],
               to = input$dates[2],
               auto.assign = FALSE)
  })
  
  adjusted_data <- reactive({
    if (!input$adjust) {
      return(dataInput())
    } else {
      return(adjust(dataInput()))
    }
  })
  
  output$plot <- renderPlot({
    chartSeries(adjusted_data(), theme = chartTheme("white"), 
                type = "line", log.scale = input$log, TA = NULL)
  })
})


# ui.R #

ui <- shinyUI(fluidPage(
  titlePanel("stockVis"),
  
  sidebarLayout(
    sidebarPanel(
      helpText("Select a stock to examine. 
        Information will be collected from yahoo finance."),
      
      textInput("symb", "Symbol", "SPY"),
      
      dateRangeInput("dates", 
                     "Date range",
                     start = "2013-01-01", 
                     end = as.character(Sys.Date())),
      
      br(),
      br(),
      
      checkboxInput("log", "Plot y axis on log scale", 
                    value = FALSE),
      
      checkboxInput("adjust", 
                    "Adjust prices for inflation", value = FALSE)
    ),
    
    mainPanel(plotOutput("plot"))
  )
))

shinyApp(ui = ui, server = server)