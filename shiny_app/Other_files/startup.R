# test script

# gets everything working the way it is supposed to


source(file = "~/Documents/dev/FlowingData/How to map geo paths in R/geo-paths/shiny_app/global.R")

# delete
date1 <- "2012-04-19"
date2 <- "2012-04-24"
#

setwd("~/Documents/dev/FlowingData/How to map geo paths in R/geo-paths/shiny_app/")

df <- read.csv(file = "~/Documents/dev/FlowingData/How to map geo paths in R/geo-paths/shiny_app/my_tracks.csv", stringsAsFactors = FALSE)

df <- cleanUp(df)

sdf <- subset(df, datetime > date1 & datetime < date2)
