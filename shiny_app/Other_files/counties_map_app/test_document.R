# Shiny App for Mini project

# app.R #

# Holy shit shiny apps are so easy and awesome
library(shiny)
# Set maximum file upload size to 1024MB (1GB)
options(shiny.maxRequestSize = 1024*1024^2)

# server.R # 

# get functions from helpers.R
source("counties_map_app/helpers.R")
# read in the R data set
counties <- readRDS("counties_map_app/counties.rds")
# load packages
library(maps)
library(mapproj)

server <- shinyServer(function(input, output) {
  output$map <- renderPlot({ # percent_map() is put in renderPlot because it should be rendered upon widget change
    datum <- switch(input$var,
                    "Percent White" = counties$white,
                    "Percent Black" = counties$black,
                    "Percent Hispanic" = counties$hispanic,
                    "Percent Asian" = counties$asian)
    percent_map(var = datum, color = "darkgreen", legend.title = "Populations", min = input$range[1], max = input$range[2])
  })
})

# ui.R #

ui <- shinyUI(fluidPage(
  titlePanel("censusVis"),
  
  sidebarLayout(
    sidebarPanel(
      helpText("Create demographic maps with 
        information from the 2010 US Census."),
      
      selectInput("var", 
                  label = "Choose a variable to display",
                  choices = c("Percent White", "Percent Black",
                              "Percent Hispanic", "Percent Asian"),
                  selected = "Percent White"),
      
      sliderInput("range", 
                  label = "Range of interest:",
                  min = 0, max = 100, value = c(0, 100))
    ),
    
    mainPanel(
      plotOutput("map")
    )
  )
))
shinyApp(ui = ui, server = server)