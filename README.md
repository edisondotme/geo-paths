## geo-paths

Takes a CSV and plots the tracks for the selected date range.

[Link to Shiny app](https://edisondotme.shinyapps.io/upload/)

In a unix terminal, type: git clone git@gitlab.com:edisondotme/geo-paths.git to copy all files to your computer to run them.

Please see the file "submission.txt" in geo-paths/submission.

That file will guide through a demo of the Rscript that is the workhorse behind the app.
